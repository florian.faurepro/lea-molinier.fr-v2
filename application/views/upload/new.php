<div class="nowrap">
	<?php echo form_open('upload/create'); ?>
		<p class="title">Créer série</p><br>

		<!-------------------- ACTIVE -------------------->
		<label for="active">Afficher la série ?</label>
		<input type="checkbox" name="active" id="active"> <br>

		<!-------------------- TITLE --------------------->
		<label for="titre">Titre : </label>
		<input type="text" name="titre" id="titre" class="inputxt" value="">
		<span class="error"> <?php echo form_error('titre'); ?></span>

		<!-------------------- TITLE-EN --------------------->
		<label for="titre">Titre (EN) : </label>
		<input type="text" name="titre-en" id="titre" class="inputxt" value="">
		<span class="error"> <?php echo form_error('titre-en'); ?></span>


		<!-------------------- SLUG --------------------->
		<label for="titre">Slug : </label>
		<input type="text" name="slug" id="slug" class="inputxt" value="">
		<span class="error"> <?php echo form_error('slug'); ?></span><br>
	
		<!------------------ DESCRIPTION ------------------>
		<label for="description">Description :</label><br>
		<textarea class="inputxt description" name="description" id="description"></textarea><br>
		<span class="error"> <?php echo form_error('description'); ?></span>

		<!------------------ DESCRIPTION - ENG ------------------>
		<label for="description">Description, but in english:</label><br>
		<textarea class="inputxt description" name="description-en" id="description-en"></textarea><br>
		<span class="error"> <?php echo form_error('description-en'); ?></span>

		<!------------------ NOM FICHIERS ------------------>
		<label for="nom_fichiers">Nom des fichiers : </label>
		<input type="text" class="inputxt" name="nom_fichiers" id="nfi" value=""><br>
		<span class="error"> <?php echo form_error('nom_fichiers'); ?></span>

		<!--------------------- M.E.P. --------------------->
		<label for="mep">Type de mise en page : </label>
		<select name="mep" id="mep">
			<option value="0">Slider</option>
			<option value="1">Mozaïque</option>
		</select>
		<span class="error"> <?php echo form_error('mep'); ?></span>

    <input class="button G" type="submit" id="submit" name="submit" value="submit">

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>

<script>
$(document).ready(function (){
	$('#titre').focusout(function(){
		var slug = string_to_slug($('#titre').val())
		$('#slug').val(slug);
		var nfi = slug;
		$('#nfi').val(nfi);
});

	function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaaeeeeiiiioooouuuunc------";

    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by - (Dash)
        .replace(/-+/g, '-'); // collapse dashes

    return str;
	};
});
</script>
