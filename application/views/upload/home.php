<?php
for ($i = 0; $i < sizeof($series); $i++) { ?>
  <div class="main">
    <!----------------- THUMBZONE ------------------>
    <div class="thumbzone">
      <?php for ($y = 1; $y < sizeof($series[$i]['photos']); $y++) { ?>
        <div>
          <div class="thumbnail">
            <img class="thumbnailImg" src="<?php echo base_url() . "uploads/" . $series[$i]['nom_fichiers'] . "/"  . $series[$i]['photos'][$y] ?>" alt="">
          </div>
          <div class="miniMenu">
            <button class="fas fa-home thumbifiy" id="thumb-<?php echo $series[$i]['slug'] . "/" . $series[$i]['nom_fichiers'] . $y ?>" value="<?php echo $series[$i]['nom_fichiers'] . "/" . $series[$i]['photos'][$y] ?>"></button>
            <input type="text" class="coucou" value="<?php echo $series[$i]['nom_fichiers'] . "/" . $series[$i]['photos'][$y]; ?>" id="<?php echo $series[$i]['slug'] . $y ?>" style="display:none">
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
<?php } ?>
<div class="main" style="justify-content: space-around;">
  <button class="submit button G">Valider</button>
  <button class="reset button R">Reset</button>
</div>

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script>
  var photosArray = [];
  var slug = [];
  $(document).ready(function() {
    $('.thumbifiy').click(function() {
      var tar = $(this).val();
      if ($(this).hasClass("fa-home")) {
        $(this).removeClass('fa-home').addClass('fa-times');
        photosArray.push(tar);
        for(i=0; i < photosArray.length; i++){
          slug[i] = photosArray[i].substring(0, photosArray[i].indexOf('/'));
        }
      } else {
        $(this).removeClass('fa-times').addClass('fa-home');
        for (i = 0; i < photosArray.length; i++) {
          if (photosArray[i] == tar) {
            slug.splice(i, 1);
            photosArray.splice(i, 1);
          }
        }
      }
      console.log(photosArray);
      console.log(slug);
    })
    $('.submit').click(function() {
      $.ajax({
        url: 'randHome',
        type: 'post',
        data: {'photos' : photosArray, 'slug' : slug},
        success: function(result) {
          console.log(result);
        }
      });
    })
    $('.reset').click(function() {
    $.ajax({
      url: 'resetHome',
      type: 'post'        
      });    
    })
  })
</script>