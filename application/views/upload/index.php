<link rel="stylesheet" href="<?php echo base_url(); ?>css/tabs.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">

<div id="tabs" class="tabs-min">
	<ul>
		<li><a href="#tabs-1">Photos</a></li>
		<li><a href="#tabs-2">Videos</a></li>
	</ul>

	<div class="list" id="tabs-1">
		<?php foreach ($series as $serie) { ?>
			<div id="<?php echo $serie['ID']; ?>" class="serie-<?php if ($serie['active'] == '0' || $serie['active'] == null){echo "in";} ?>active drag">
				<span class="title"><?php echo "#" . $serie['ID'] . " " . $serie['titre'] ?></span>	
				<div style="display : flex">
					<div class="button G" title="Edit serie"><a href="<?php echo base_url() . "upload/" . $serie['slug']; ?>"><i class="far fa-edit">E</i></a></div>	
					<div class="button G" title="Add photos"><a href="<?php echo base_url() . "upload/add?slug=" . $serie['slug'] ?>"><i class="far fa-images">P</i></i></a></div>	
					<div class="button R" title="Delete serie"><a href="<?php echo base_url() . "upload/delete?slug=" . $serie['slug'] ?>"><i class="fas fa-trash">D</i></a></div>	
				</div>

			</div><br>
		<?php }?>

		<div class="button G"><a href="<?php echo base_url() . "upload/new" ?>">New serie</a></div>	
		<div class="button G"><a href="<?php echo base_url() . "upload/home" ?>">Home backgrounds</a></div>	
		<div class="button G" id="btn-sre-rdr">Reorder</div>	
	</div>

	<div class="list" id="tabs-2">
		<?php foreach ($videos as $video) { ?>
			<div id="<?php $video['id'] ?>" class="serie-<?php if ($video['active'] == '0' || $video['active'] == null){echo "in";} ?>active ">
				<span class="title"><?php echo "#" . $video['id'] . " " . $video['name'] ?></span>	
				<div style="display : flex">
					<div class="button G" title="Edit video page"><a href="<?php echo base_url() . "upload/video/" . $video['slug']; ?>"><i class="far fa-edit">E</i></a></div>	
					<div class="button R" title="delete video"><a href="<?php echo base_url() . "upload/delete_video?slug=" . $video['slug'] ?>"><i class="fas fa-trash">D</i></a></div>	
				</div>

			</div><br>
		<?php }?>

		<div class="button G"><a href="<?php echo base_url() . "upload/new_video" ?>">New video</a></div>	
	</div>

</div>

<script src="https://kit.fontawesome.com/d1e969dc9b.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
	$(function() {
		$("#tabs").tabs();
	});
</script>

<script>
	$(document).ready(function() {
		var dropIndex;
		$(".list").sortable({
			update: function(event, ui) {
				dropIndex = ui.item.index();
			}
		});

		$('#btn-sre-rdr').click(function(e) {
			var imageIdsArray = [];

			$('.drag').each(function(index) {
				var id = $(this).attr('id');
				imageIdsArray.push(id);
			});
			 console.log("ids : " + imageIdsArray )

			$.ajax({
				url: 'reorderSeries',
				type: 'post',
				data: {
					'seriesOrder': imageIdsArray,
				},
				success: function(result) {
					console.log(result);
					if (result == "true") {
						var modalTitle = 'Success !';
						var modalMsg = 'Les series ont bien été réorganisées. </br> Cliquez n\'importe où, ou appuyez sur la touche "F5" pour recharger la page';
						ShowModal(modalTitle, modalMsg);
					}
				}
			});
		});

		function ShowModal(modalTitle, modalMsg) {
			$("#modal").toggle();
			$("#modal-msg").html(modalMsg);
			console.log('ok');
		}

		$("#modal").click(function() {
			window.location.reload();
		})
	})
</script>