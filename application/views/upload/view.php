<link rel="stylesheet" href="<?php echo base_url(); ?>css/modal.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/tabs.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>lib/dropzone/dropzone.css">

<div class="container">

	<div class="button Y"><a href="<?php echo base_url() . "upload" ?>">Return</a></div>

	<div id="tabs" class="tabs-min">
		<ul>
			<li><a href="#tabs-1">Paramètres de la série</a></li>
			<li><a href="#tabs-2">Ordre des photos</a></li>
			<li><a href="#tabs-3">Ajout de photos</a></li>
		</ul>

		<div id="tabs-1">
			<div class="container">
				<?php echo form_open('upload/update'); 	?>
				<p class="title">Modifier la série : <?php echo $serie['titre'] ?></p> <input type="text" id="serieId" name="serieId" value="<?php echo $serie['ID'] ?>" style="display: none"><br>

				<table class="form-table">
					<tr>
						<!-------------------- ACTIVE -------------------->
						<td class="txt-right mr-1">Afficher la série ?</td>
						<td>
							<input type="checkbox" name="active" id="active" <?php if ($serie['active'] == 1) {
																																	echo "checked";
																																} ?> class="largerCheckbox">
						</td>
					</tr>
					<tr>
						<!-------------------- TITLE --------------------->
						<td class="txt-right mr-1">Titre : </td>
						<td>
							<input type="text" name="titre" id="titre" class="inputxt" value="<?php echo $serie['titre'] ?>">
							<span class="error"> <?php echo form_error('titre'); ?></span>
						</td>
					</tr>
					<tr>
						<!-------------------- TITLE-EN --------------------->
						<td class="txt-right mr-1">Title : </td>
						<td>
							<input type="text" name="titre-en" id="titre" class="inputxt" value="<?php echo $serie['titre-en'] ?>">
							<span class="error"> <?php echo form_error('titre-en'); ?></span>
						</td>
					</tr>
					<tr>
						<!------------------ DESCRIPTION ------------------>
						<td class="txt-right mr-1" style="vertical-align: top;">Description :</td>
						<td>
							<textarea class="form-desc" name="description" id="description"><?php echo $serie['description'] ?></textarea><br>
							<span class="error"> <?php echo form_error('description'); ?></span>
						</td>
					</tr>
					<tr>
						<!------------------ DESCRIPTION-EN ------------------>
						<td class="txt-right mr-1" style="vertical-align: top;">Description, but in english :</td>
						<td>
							<textarea class="form-desc" name="description-en" id="description"><?php echo $serie['description-en'] ?></textarea><br>
							<span class="error"> <?php echo form_error('description-en'); ?></span>
						</td>
					</tr>
					<tr>
						<!------------------- THUMBNAIL ------------------->
						<td class="txt-right mr-1">Thumbnail :</td>
						<td>
							<select name="thumbnail" id="thumbnail">
								<option value="<?php echo $serie['thumbnail'] ?>">actuel : <?php echo $serie['thumbnail'] ?></option>
								<?php for ($i = 0; $i < sizeof($serie['photos']); $i++) { ?>
									<option value="<?php echo $serie['photos'][$i] ?>"><?php echo $serie['photos'][$i]; ?></option>
								<?php } ?>
							</select>
							<span class="error"> <?php echo form_error('thumbnail'); ?></span>
						</td>
					</tr>
					<tr>
						<!--------------------- M.E.P. --------------------->
						<td class="txt-right mr-1">Type de mise en page : </td>
						<td>
							<select name="mep" id="mep">
								<option value="0">Slider</option>
								<option value="1">Mozaïque</option>
							</select>
							<span class="error"> <?php echo form_error('mep'); ?></span><br>
						</td>
					</tr>
					<tr>
						<td class="txt-right mr-1">
							<!--------------------- SUBMIT --------------------->
							<input class="button G" type="submit" id="submit" name="submit" value="Submit">
						</td>
						<td>
							<div class="buttonDiv R" id="cancelBtn">Cancel</div>
						</td>
					</tr>
				</table>



				</form>
			</div>
		</div>

		<div id="tabs-2">
			<!----------------- THUMBZONE ------------------>
			<div class="thumbzone">
				<?php for ($i = 0; $i < sizeof($serie['photos']); $i++) { ?>
					<div>
						<div class="thumbnail" id="<?php echo $serie['photos'][$i] ?>">
							<img class="thumbnailImg" src="<?php echo "../uploads/" . $serie['nom_fichiers'] . "/"  . $serie['photos'][$i] ?>" alt="">
						</div>
						<div class="miniMenu">
							<i class="far fa-edit thumbifiy"></i>
						</div>
					</div>
				<?php } ?>
			</div>
			<div id="btn-img-rdr" class="button G">Validate</div>
		</div>

		<!----------------- PHOTOS DROPZONE ------------------>
		<div id="tabs-3">
			<div id="container">
				<h1>Add a bunch of photos here.</h1>
				<div id="body">
					<form method="post" action="uploads?slug=<?php echo $serie['slug'] ?>" enctype="multipart/form-data" class="dropzone" id="myAwesomeDropzone"> </form>
					<button class="button G" type="button" id="submit_dropzone_form">Upload</button>
					<div class="final-info">
						<label for="uploaded_files">Response recieved : </label>
						<input type="text" id="uploaded_files">
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="modal-bkg" style="display: none" id="modal">
	<div class="modal">
		<span class="message" id="modal-msg">placeholder</span>
	</div>
</div>


<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>


<script>
	$(function() {
		$("#tabs").tabs();
	});
</script>

<script>
	$(document).ready(function() {
		$('.thumbify').click(function() {
			$.ajax({
				url: 'script.php?argument=value&foo=bar'
			});
		})
	})
</script>


<script>
	var modalTitle;
	var modalMsg;

	$(document).ready(function() {
		var dropIndex;
		$(".thumbzone").sortable({
			update: function(event, ui) {
				dropIndex = ui.item.index();
			}
		});

		$('#btn-img-rdr').click(function(e) {
			var imageIdsArray = [];
			var slug = '<?php echo $serie['slug'] ?>';

			$('.thumbnail').each(function(index) {
				var id = $(this).attr('id');
				imageIdsArray.push(id);
			});

			$.ajax({
				url: 'ReorderImages',
				type: 'post',
				data: {
					'imagesOrder': imageIdsArray,
					'slug': slug
				},
				success: function(result) {
					console.log(result);
					if (result == "true") {
						modalTitle = 'Success !';
						modalMsg = 'Les images ont bien été réorganisées. </br> Cliquez n\'importe où, ou appuyez sur la touche "F5" pour recharger la page';
						ShowModal(modalTitle, modalMsg);
					}
				}
			});
		});

		function ShowModal(modalTitle, modalMsg) {
			$("#modal").toggle();
			$("#modal-msg").html(modalMsg);
		}

		$("#modal").click(function() {
			window.location.reload();
		})
	})
</script>

<script src="<?php echo base_url() . "lib/dropzone/dropzone.js" ?>"></script>
<script>
	Dropzone.options.myAwesomeDropzone = {
		autoProcessQueue: false,
		uploadMultiple: true,
		parallelUploads: 5,
		successmultiple: function(data, response) {
			$("#uploaded_files").val(response);
		},
		init: function() {
			//Submitting the form on button click
			var submitButton = document.querySelector("#submit_dropzone_form");
			myDropzone = this; // closure
			submitButton.addEventListener("click", function() {
				myDropzone.processQueue(); // Tell Dropzone to process all queued files.
			});
		}
	};
</script>

<script>
	$(document).ready(function() {
		$('#cancelBtn').click(function() {
			window.location.reload();
		})
	})
</script>