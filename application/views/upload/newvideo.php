<link rel="stylesheet" href="<?php echo base_url(); ?>lib/dropzone/dropzone.css">

<div class="container">
  <div class="button Y"><a href="<?php echo base_url() . "upload" ?>">Return</a></div>

 
  <?php 
  if(validation_errors())
  {
    echo '<div id="err" class="R" style="color: white"> The submission failed with the following error(s):';
    echo validation_errors(); 
    echo '</div>';
  }
  ?>
 

  <?php echo form_open_multipart('upload/add_video');   ?>
  <table class="form-table">
    <tr>
      <!-------------------- ACTIVE -------------------->
      <td class="txt-right mr-1">Afficher la video ?</td>
      <td>
        <input type="checkbox" name="active" id="active" class="largerCheckbox">
      </td>
    </tr>
    <tr>
      <!-------------------- TITLE --------------------->
      <td class="txt-right mr-1">Titre : </td>
      <td>
        <input type="text" name="name" id="name" class="inputxt" value="<?php echo set_value('name'); ?>">
        <input type="hidden" name="id" value="">
      </td>
    </tr>
    <tr>
      <td colspan="2" >
        <span class="error"> <?php echo form_error('name'); ?></span>
      </td>
    </tr>
    <tr>
      <!-------------------- TITLE-EN --------------------->
      <td class="txt-right mr-1">Titre-en : </td>
      <td>
        <input type="text" name="name-en" id="name" class="inputxt" value="<?php echo set_value('name-en'); ?>">
        <input type="hidden" name="id" value="">
      </td>
    </tr>
    <tr>
      <td colspan="2" >
        <span class="error"> <?php echo form_error('name'); ?></span>
      </td>
    </tr>
    <!-------------------- LINK --------------------->
    <td class="txt-right mr-1">Lien : </td>
    <td>
      <span>https://vimeo.com/</span>
      <input type="text" name="link" id="link" class="inputxt" value="">
    </td>
    </tr>
    <tr>
      <td colspan="2" >
        <span class="error"> <?php echo form_error('link'); ?></span>
      </td>
    </tr>
    <tr>
      <!------------------ DESCRIPTION ------------------>
      <td class="txt-right mr-1" style="vertical-align: top;">Description :</td>
      <td>
        <textarea class="form-desc" name="description" id="description"></textarea><br>
        <span class="error"> <?php echo form_error('description'); ?></span>
      </td>
    </tr>
    <tr>
      <!------------------ DESCRIPTION EN ------------------>
      <td class="txt-right mr-1" style="vertical-align: top;">Description :</td>
      <td>
        <textarea class="form-desc" name="description-en" id="description-en"></textarea><br>
        <span class="error"> <?php echo form_error('description-en'); ?></span>
      </td>
    </tr>
    <tr>
      <!------------------- THUMBNAIL ------------------->
      <td class="txt-right mr-1">Thumbnail :</td>
      <td>
        <input type="file" name="userfile" id="userfile"><br>
        <Span style="color: red">the thumbnail image must be between 300px wide and 200px high, jpeg or png.</Span>
        <span class="error"> <?php echo form_error('thumbnail'); ?></span>
      </td>
    </tr>
    <tr>
      <td class="txt-right mr-1">
        <!--------------------- SUBMIT --------------------->
        <input class="button G" type="submit" id="submit" name="submit" value="Submit">
      </td>
      <td>
        <div class="buttonDiv R" id="cancelBtn">Cancel</div>
      </td>
    </tr>
  </table>
  </form>
</div>

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
  $(document).ready(function() {
    $('#cancelBtn').click(function() {
      window.location.reload();
    })
  })
</script>

