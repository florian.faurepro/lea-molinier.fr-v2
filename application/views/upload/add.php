<link rel="stylesheet" href="<?php echo base_url()."lib/dropzone/dropzone" ?>.css">

<div id="container">
	<h1>Add a bunch of photos here.</h1>
	<div id="body">
		<form method="post" action="uploads?slug=<?php echo $_GET['slug'] ?>" enctype="multipart/form-data" class="dropzone" id="myAwesomeDropzone">		</form>
		<button class="button G" type="button" id="submit_dropzone_form">Upload</button>
		<div class="button Y"><a href="<?php echo base_url() . "upload" ?>">Return</a></div>	
		<div class="final-info">
			<label for="uploaded_files">Response recieved : </label>
			<input type="text" id="uploaded_files">
		</div>
	</div>
</div>

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="<?php echo base_url() . "lib/dropzone/dropzone.js" ?>"></script>

<script>
Dropzone.options.myAwesomeDropzone = {
	autoProcessQueue: false,
	uploadMultiple: true,
	parallelUploads:5,
	successmultiple:function(data,response){
		$("#uploaded_files").val(response);
	},
	init: function() {
		//Submitting the form on button click
		var submitButton = document.querySelector("#submit_dropzone_form");
			myDropzone = this; // closure
			submitButton.addEventListener("click", function() {
			myDropzone.processQueue(); // Tell Dropzone to process all queued files.
		});
	}
};
</script>