<div class="container">
  <div class="button Y"><a href="<?php echo base_url() . "upload" ?>">Return</a></div>

  
<?php if (isset($msg) && $msg != NULL && isset($msg['thumbresult']['error']))
      { ?>
        <div id="msg" class="Y">
          <?php echo $msg['txt'] . "<br>" . $msg['thumbresult']['error']; ?>
        </div>
<?php }
      elseif (isset($msg) && $msg != NULL && !isset($msg['thumbresult']['error']))
      { ?>
        <div id="msg" class="G">
          <?php echo $msg['txt'] . "<br> <p> Great success !! </p>" ?>
        </div>
<?php }
  echo form_open('upload/update_video');?>
    <table class="form-table">
      <tr>
        <!-------------------- ACTIVE -------------------->
        <td class="txt-right mr-1">Afficher la video ?</td>
        <td>
          <input type="checkbox" name="active" id="active" <?php if ($video['active'] == 1) {
                                                              echo "checked";
                                                            } ?> class="largerCheckbox">
        </td>
      </tr>
      <tr>
        <!-------------------- TITLE --------------------->
        <td class="txt-right mr-1">Titre : </td>
        <td>
          <input type="text" name="name" id="name" class="inputxt" value="<?php echo $video['name'] ?>">
          <input type="hidden" name="id" value="<?php echo $video['id'] ?>">
        </td>
      </tr>
      <tr>
        <td colspan="2" >
          <span class="error"> <?php echo form_error('name'); ?></span>
        </td>
      </tr>
      <tr>
        <!-------------------- TITLE-EN --------------------->
        <td class="txt-right mr-1">Titre EN : </td>
        <td>
          <input type="text" name="name-en" id="name-en" class="inputxt" value="<?php echo $video['name-en'] ?>">
        </td>
      </tr>
      <tr>
        <td colspan="2" >
          <span class="error"> <?php echo form_error('name-en'); ?></span>
        </td>
      </tr>
      <!-------------------- LINK --------------------->
      <td class="txt-right mr-1">Lien : </td>
        <td>
          <span>https://vimeo.com/</span> 
          <input type="text" name="link" id="link" class="inputxt" value="<?php echo $video['link'] ?>">
        </td>
      </tr>
      <tr>
        <td colspan="2" >
          <span class="error"> <?php echo form_error('link'); ?></span>
        </td>
      </tr>
      <tr>
        <!------------------ DESCRIPTION ------------------>
        <td class="txt-right mr-1" style="vertical-align: top;">Description :</td>
        <td>
          <textarea class="form-desc" name="description" id="description"><?php echo $video['description'] ?></textarea><br>
          <span class="error"> <?php echo form_error('description'); ?></span>
        </td>
      </tr>
      <tr>
        <!------------------ DESCRIPTION-EN ------------------>
        <td class="txt-right mr-1" style="vertical-align: top;">Description EN :</td>
        <td>
          <textarea class="form-desc" name="description-en" id="description-en"><?php echo $video['description-en'] ?></textarea><br>
          <span class="error"> <?php echo form_error('description-en'); ?></span>
        </td>
      </tr>
      <tr>
        <!------------------- THUMBNAIL ------------------->
        <td class="txt-right mr-1">Thumbnail :</td>
        <td>
          <select name="thumbnail" id="thumbnail">
            <option value="<?php echo $video['thumbnail'] ?>">actuel : <?php echo $video['thumbnail'] ?></option>
            <?php foreach ($video['thumbs'] as $row) { ?>
              <option value="<?php echo $row ?>"><?php echo $row; ?></option>
            <?php } ?>
          </select>
          <input type="file" name="userfile" id="userfile">
          <span class="error"> <?php echo form_error('thumbnail'); ?></span>
        </td>
      </tr>
      <tr>
        <td class="txt-right mr-1">
          <!--------------------- SUBMIT --------------------->
          <input class="button G" type="submit" id="submit" name="submit" value="Submit">
        </td>
        <td>
          <div class="buttonDiv R" id="cancelBtn">Cancel</div>
        </td>
      </tr>
    </table>
  </form>
</div>

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
	$(document).ready(function() {
		$('#cancelBtn').click(function() {
			window.location.reload();
		})
  });
  
  $(document).ready(function() {
		$('#msg').click(function() {
			$('#msg').hide();
		})
	})
</script>