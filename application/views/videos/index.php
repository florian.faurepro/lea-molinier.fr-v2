<div class="main">

  <?php
  if(!isset($_SESSION['language'])) $_SESSION['language'] = "fr";

  foreach ($videos as $video) {
    $thumbnailNumber = $video['thumbnail']; ?>
    <div class="serie">
      <div class="preview">
        <a href="<?php echo base_url() . "videos/" . $video['slug']; ?>">
        <img class="thumbnailImg" src="<?php echo base_url() . "uploads/videos-thumbnails/" . $video['slug'] ."/". $video['thumbnail'] ?>">'; 
        <!-- <span class="previTit"><?php // echo $video['titre'] ?></span> -->
        </a>
      </div>
      <div class="header module line-clamp">        
          <span><?php echo ($_SESSION['language'] == "fr" || $video['name-en'] == "") ? $video['name'] . " - " :  $video['name-en'] . " - "; 
          echo ($_SESSION['language'] == "fr" || $video['description-en'] == "") ? $video['description'] : $video['description-en'] ; ?></span>
      </div>
    </div>
  <?php } ?>
</div>