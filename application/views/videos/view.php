<div class="mainSlider">
  <div class="desc" style="max-width: 15%">
    <span style="white-space: pre-line"><?php echo (lang() == "en") ? $video['description-en'] : $video['description']; ?></span>
  </div>
  <iframe 
    id="playa"
    title="vimeo-player"  
    src="https://player.vimeo.com/video/<?php echo $video['link'];?>" 
    frameborder="0"
    width="" 
    height="">
  </iframe>
</div>

<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script>
  const vw = jQuery(window).width()/2;
  const vh = jQuery(window).height()/2;

  var playerW = vw / 2;
  var playerH = vh / 2;

  $('#playa').width(vw);
  $('#playa').height(vh);  
</script>