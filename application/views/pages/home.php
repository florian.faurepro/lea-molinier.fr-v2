<body class="coucou">
<?php $rand = rand(0, sizeof($serie) - 1); ?>
<span class="botLink">*dans la série <a href="series/<?php  echo $serie[$rand]['slug'] ?>"><?php  echo $serie[$rand]['slug']?></a></span>

  <script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
  <script src="<?php echo base_url() . "lib/swiper-master/dist/js/swiper.min.js" ?>" type="text/javascript" charset="utf-8"></script>
  <script>
    $(document).ready(function() {   // quand la page est chargée
      if ($(window).width() < 767) { // si l'écran est moins large de 768px
        $(".coucou").css("background-image", 'url(./uploads/terre-de-legendes/Molinier_Rep_09.jpg)'); // mettre "Molinier_Rep_09.jpg" en background image
        $('.botLink').html('*Dans la série <a href="series/peregrinations-photographiques">Pérégrinations Photographiques</a>');
      } else { // sinon
        test_version(); // *appelle une autre fonction*
        $(".coucou").css("background-image", 'url(./uploads/<?php echo $serie[$rand]['src']; ?>)'); // mettre en bcg-img une des images au hasard parmis celles contenues dans 
                                                                                                    // la variable $serie['src']
      }
    });
    function test_version(){
      if (/MSIE 10/i.test(navigator.userAgent)) {
      // This is internet explorer 10
        window.alert(`Welcome to Lea-molinier.fr, for an optimal experience, please consider using an actually functioning Web Browser such as 
        Chrome, Firefox, or anything else than Edge or Internet explorer.`);
      }

      if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
          // This is internet explorer 9 or 11
        window.alert(`Welcome to Lea-molinier.fr, for an optimal experience, please consider using an actually functioning Web Browser such as 
        Chrome, Firefox, or anything else than Edge or Internet explorer.`);
      }

      if (/Edge\/\d./i.test(navigator.userAgent)){
        // This is Microsoft Edge
        window.alert(`Welcome to Lea-molinier.fr, for an optimal experience, please consider using an actually functioning Web Browser such as 
        Chrome, Firefox, or anything else than Edge or Internet explorer.`);
      }
    }
   
  </script>
  