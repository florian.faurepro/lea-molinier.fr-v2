<div class="mainAbout">
  <div class="about"> 
    <img src="./resources/portrait.png" alt="" class="portrait">
    <?php 
    if(!isset($_SESSION['language'])) $_SESSION['language'] = "fr";
    $en = "<div class=\"about-text\">
            <p>Lea Molinier is a photographer from Toulouse, France and currently based in Glasgow, Scotland.</p> 
            <p><strong>2018</strong>: 1st exhibition of the series Urban Mirage in Albi, France </p>                
            <p><strong>2019</strong>: Graduated from the ETPA photography school in Toulouse, France </p>     
            <p><strong>2020</strong>: 
              Volunteer photographer for the Govan Community Project charity, Glasgow, Scotland<br>
              Broadcasting of the Kid Valley 8 video as part of the Pop Mutations virtual festival, Glasgow, Scotland<br>
              Online exhibition during the Scottish Mental Health Arts Festival, Glasgow, Scotland</p>  
          </div>";

    $fr = "<div class=\"about-text\">
            <p>Léa Molinier est une photographe originaire de la région de Toulouse, France et vivant à Glasgow, Ecosse.</p>
            <p><strong>2018</strong> : 1ère exposition de la série Mirage Urbain à Albi, France    </p>
            <p><strong>2019</strong> : Diplômée de l'école de photographie ETPA à Toulouse, France    </p>
            <p><strong>2020</strong> : Photographe bénévole pour l'ONG Govan Community Project, Glasgow, Ecosse<br>
              Diffusion de la vidéo Kid Valley 8 pendant le festival virtuel Pop Mutations, Glasgow, Ecosse<br>
              Exposition en ligne lors du Festival Ecossais des Arts et de la Santé Mentale, Glasgow, Ecosse</p>
          </div>";

    echo ($_SESSION['language'] == "fr") ? $fr : $en;
    ?>
  </div>
  <div class="contact">
    <span style="">contact for collaboration or any inquieries at : <br> l-molinier@outlook.fr <br> follow ongoing work on Instagram : <br> @leamophotographie</span>
  </div>
</div>