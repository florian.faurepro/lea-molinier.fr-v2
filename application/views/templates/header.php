<?php 

if (isset($_GET['lang'])) $_SESSION['language'] = $_GET['lang'];
else $_SESSION['language'] = "fr";
function lang()
{
    return (isset($_SESSION['language'])) ? $_SESSION['language'] : "fr" ;
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Léa Molinier photographie</title>
    <script src="https://kit.fontawesome.com/d1e969dc9b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>lib/swiper-master/dist/css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"> <META HTTP-EQUIV="Expires" CONTENT="-1"> 
</head>

<header>
    <div class="signature">
    <img class="signature" src="<?php echo base_url() ?>resources/signature.png" alt="">
    </div>
    <?php 
    $userdata = $this->session->userdata;
    
    echo (isset($userdata['user']) && $userdata['user'] == 'Risu' && $userdata['email'] == 'l-molinier@outlook.fr' && $userdata['logged_in'] == TRUE) ?
    '<div class="cog"><a href="'. base_url() .'upload"><img src="' . base_url() . 'resources/cogW.png" alt="" class="cogimg"></a></div>' 
    : ""; ?>
    <div class="headerContainer ff-nixie fs-l">
        <ul class="h-link-list">
            <li class="h-li "><a class="white" href="<?php echo base_url() . '">'; echo (lang() == "en") ? 'Home' : 'Accueil'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-li "><a class="white" href="<?php echo base_url() . 'series">'; echo (lang() == "en") ? 'Photographs' : 'Photographie'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-li "><a class="white" href="<?php echo base_url() . 'videos">'; echo (lang() == "en") ? 'Videos' : 'Vidéo'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-li "><a class="white" href="<?php echo base_url() . 'about">'; echo (lang() == "en") ? 'About / Contact' : 'À propos / Contacts'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-li"><a class="white" href="https://www.instagram.com/leamophotographie/" title="Instagram @leamophotographie"><i class="fab fa-instagram"></i></a></li>
            <li class="ghost-li"> </li>
            <li class="h-li mob-hidden"><a class="white fs1 mob-hidden" href="<?php echo current_url() . '?lang=en';?> ">EN</a></li>     
            <li class="ghost-li fs1 white mob-hidden"> / </li>
            <li class="h-li mob-hidden"><a class="white fs1 mob-hidden" href="<?php echo current_url() . '?lang=fr';?> ">FR</a></li>     
            <li class="h-li mob-block"><a class="white mob-inline" href="<?php echo current_url() . '?lang=en';?> ">EN</a> <span class="white mob-inline"> / </span><a class="white mob-inline" href="<?php echo current_url() . '?lang=fr';?> ">FR</a></li>     

        </ul>
    </div>
</header>

<body>