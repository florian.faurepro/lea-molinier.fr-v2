<?php 

if (isset($_GET['lang'])) $_SESSION['language'] = $_GET['lang'];

function lang()
{
    return (isset($_SESSION['language'])) ? $_SESSION['language'] : "fr" ;
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Léa Molinier photographie</title>
    <script src="https://kit.fontawesome.com/d1e969dc9b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>lib/swiper-master/dist/css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"> <META HTTP-EQUIV="Expires" CONTENT="-1"> 
</head>

<header>
    <div>
        <img class="signature" src="<?php echo base_url() ?>/resources/signatureB.png" alt="">
    </div><?php 
    $userdata = $this->session->userdata;
    
    echo (isset($userdata['user']) && $userdata['user'] == 'Risu' && $userdata['email'] == 'l-molinier@outlook.fr' && $userdata['logged_in'] == TRUE) ?
    '<div class="cog"><a href="'. base_url() .'upload"><img src="' . base_url() . 'resources/cog.png" alt="" class="cogimg"></a></div>' 
    : ""; ?>
    <div class="headerContainer ff-nixie fs-l ">
        <ul class="h-link-listB">
            <li class="h-liB"><a class="black" href="<?php echo base_url() . '">'; echo (lang() == "en") ? 'Home' : 'Accueil'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-liB"><a class="black" href="<?php echo base_url() . 'series">'; echo (lang() == "en") ? 'Photographs' : 'Photographie'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-liB "><a class="black" href="<?php echo base_url() . 'videos">'; echo (lang() == "en") ? 'Videos' : 'Vidéo'; ?></a></li>
            <li class="ghost-li"> </li>
            <li class="h-liB"><a class="black" href="<?php echo base_url() . 'about">'; echo (lang() == "en") ? 'About / Contact' : 'À propos / Contacts'; ?></a></li>
            <li class="ghost-li mob-hidden"> </li>
            <li class="h-liB mob-hidden"><a class="black" href="https://www.instagram.com/leamophotographie/"><i class="fab fa-instagram"></i></a></li>
            <li class="ghost-li"> </li>
            <li class="h-liB"><a class="black fs1" href="<?php echo current_url() . '?lang=en';?> ">EN</a></li>     
            <li class="ghost-li fs1"> / </li>
            <li class="h-liB"><a class="black fs1" href="<?php echo current_url() . '?lang=fr';?> ">FR</a></li>     
        </ul>
    </div>
</header>

<body>