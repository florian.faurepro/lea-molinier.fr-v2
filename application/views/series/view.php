
<div class="mainSlider">
	<div class="desc" >
		<span style="white-space: pre-line"><?php echo (lang() == "en") ? $serie['description-en'] : $serie['description']; ?></span>
	</div>
	<div class="swiper-container">	
	<div class="swiper-button-next swiper-button-white"></div>
	<div class="swiper-button-prev swiper-button-white"></div>
			<div class="swiper-wrapper">
				<?php for($i = 0; $i < sizeof($serie['photos']) ; $i++)
				{ ?>
					<div class="swiper-slide" >
						<img class="swiperImg" src="<?php echo base_url() . "uploads/" . $serie['nom_fichiers'] . "/"  . $serie['photos'][$i] ?>"  alt="" style="height:500px" >
					</div>
				<?php } ?>			
		</div>
	</div>
</div>

<div class="mobile-main">
	<?php for($i = 0; $i < sizeof($serie['photos']) ; $i++)
		{ ?>					
			<img class="mobile-img" src="<?php echo base_url() . "uploads/" . $serie['nom_fichiers'] . "/"  . $serie['photos'][$i] ?>"  alt="">				
	<?php } ?>
</div>


<script src="<?php echo base_url() . "lib/jq.js" ?>" type="text/javascript"></script>
<script src="<?php echo base_url() . "lib/swiper-master/dist/js/swiper.min.js" ?>" type="text/javascript" charset="utf-8"></script>

<script>
	$(document).ready(function(){
		if ($(window).width() < 1100) {
			var swiper = new Swiper({
				el: '.swiper-container',
				initialSlide: 0,
				direction: 'vertical',
				spaceBetween: 0,
				slidesPerView: 'auto',
				centeredSlides: true,
				slideToClickedSlide: true,
				grabCursor: true,
				autoHeight: true,
				mousewheel: {
					enabled: false,
				},
				keyboard: {
					enabled: true,
				},
				pagination: {
					el: '.swiper-pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
		});
		} else {
			var swiper = new Swiper({
				el: '.swiper-container',
				initialSlide: 0,
				spaceBetween: 0,
				slidesPerView: 'auto',
				centeredSlides: false,
				slideToClickedSlide: true,
				grabCursor: true,
				mousewheel: {
					enabled: false,
				},
				keyboard: {
					enabled: true,
				},
				pagination: {
					el: '.swiper-pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
		});
		}
	})
		
</script>
