<div class="main">

  <?php
  if(!isset($_SESSION['language'])) $_SESSION['language'] = "fr";
  foreach ($series as $serie) {
    $thumbnailNumber = $serie['thumbnail']; ?>
    <div class="serie">
      <div class="preview">
        <a href="<?php echo base_url() . "series/" . $serie['slug']; ?>">
        <img class="thumbnailImg" src="<?php echo base_url() . "uploads/" . $serie['nom_fichiers'] . "/" . $serie['thumbnail'] ?>">'; 
        <!-- <span class="previTit"><?php // echo $serie['titre'] ?></span> -->
        </a>
      </div>
      <div class="header module line-clamp">        
          <span><?php echo ($_SESSION['language'] == "fr" || $serie['titre-en'] == "") ? $serie['titre'] . " - " :  $serie['titre-en'] . " - "; 
          echo ($_SESSION['language'] == "fr" || $serie['description-en'] == "") ? $serie['description'] : $serie['description-en'] ; ?>  </span>
      </div>
    </div>
  <?php } ?>
</div>