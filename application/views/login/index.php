<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href='./css/style.css'>
</head>
<body>
    <div class="main">
        <div class="formframe">
            <?php echo form_open_multipart('login/login_validation');?>
            
            <span> <?php echo form_error('username'); ?></span>
            <label for="username">Username</label>
            <input type="text" name="username">
            <br>
            <span> <?php echo form_error('password'); ?></span>
            <label for="password">Password</label>
            <input type="password" name="password">
            <br>
            <?php echo $this->session->flashdata('error'); ?> 
            <br>
            <input type="submit" value="login" class="button G"/> <br>

        </div>
    </div>
    
</body>
</html>