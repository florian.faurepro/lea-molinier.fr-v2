<?php
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('login_model');
        $this->load->helper(array('form_helper', 'url_helper'));
        
    }

    public function index()
    {        
        $this->load->view('login/index');
    }

    public function login_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()) 
        {
            $username = hash('adler32', $this->input->post('username'));
            $password = hash('adler32', $this->input->post('password'));


            $this->load->model('Login_model');
            if ($this->Login_model->can_login($username, $password)) 
            {
                $session_data = array(
                    'username' => $username
                );
                $this->session->set_userdata($session_data);
                redirect('upload');
            } 
            else 
            {
                $this->session->set_flashdata('error', 'Invalind username or password'); 
                redirect('login');
            }
        } 
        else 
        {
            redirect('login');
        }
    }

    public function enter()
    {
        if($this->session->userdata('username' != ''))
        {

        }
        else
        {
            redirect(base_url() . 'login');
        }
    }
}
