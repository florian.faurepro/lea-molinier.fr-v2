<?php
class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('series_model');
		$this->load->helper(array('form', 'url_helper'));
		$this->load->library('session');
	}

	public function view($slug = 'home')
	{
		if (!file_exists(APPPATH . 'views/pages/' . $slug . '.php')) 
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data['serie'] = $this->series_model->get_randy();
		// $data['serie']['photos'] = $this->series_model->browse_files($data['serie']['nom_fichiers']);
		if($slug == 'home') {
			$this->load->view('templates/header', $data);
			$this->load->view('pages/' . $slug, $data);
			$this->load->view('templates/footer', $data);
		}else{
			$this->load->view('templates/headerB', $data);
			$this->load->view('pages/' . $slug, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function home($slug)
	{
		// $data['serie'] = $this->series_model->get_serie($slug);
		// $data['serie']['photos'] = $this->series_model->browse_files($data['serie']['nom_fichiers']);
	}
}
