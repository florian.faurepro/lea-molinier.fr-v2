<?php
class Videos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('videos_model');
		$this->load->helper(array('form', 'url_helper'));
		$this->load->library('session');
	}

	public function index()
	{
		$data['videos'] = $this->videos_model->get_videos();
		$data['title'] = 'Videos';

		$this->load->view('templates/headerB');
		$this->load->view('videos/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug = NULL)
	{
		$data['video'] = $this->videos_model->get_video($slug);
		
		if (empty($data['video'])) {
			show_404();
		}

		$data['titre'] = $data['video']['name'];

		$this->load->view('templates/headerB', $data);
		$this->load->view('videos/view', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{           
		if ($this->session->userdata('username'))
		{

			$this->load->helper('form');                
			$this->load->library('form_validation');
			
			$data['title'] = 'Create a news item';
			
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			
			if ($this->form_validation->run() === FALSE  ) 
			{
				$this->load->view('templates/headerB', $data);
				$this->load->view('news/create');
				$this->load->view('upload/upload_form');
				$this->load->view('templates/footer');
			} 
			else 
			{
				$this->news_model->set_news();
				$this->load->view('news/success');
							
			}
		}  
		else
		{
			redirect('login');
		}
	}
}
