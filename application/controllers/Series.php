<?php
class Series extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('series_model');
		$this->load->helper(array('form', 'url_helper'));
		$this->load->library('session');
	}

	public function index()
	{
		$data['series'] = $this->series_model->get_series();
		$data['title'] = 'Séries';

		$this->load->view('templates/headerB');
		$this->load->view('series/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug = NULL)
	{
		$data['serie'] = $this->series_model->get_serie($slug);
		$data['serie']['photos'] = $this->series_model->browse_files($data['serie']['nom_fichiers']);
		
		if (empty($data['serie'])) {
			show_404();
		}

		$data['titre'] = $data['serie']['titre'];

		$this->load->view('templates/headerB', $data);
		$this->load->view('series/view', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{           
		if ($this->session->userdata('username'))
		{

			$this->load->helper('form');                
			$this->load->library('form_validation');
			
			$data['title'] = 'Create a news item';
			
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			
			if ($this->form_validation->run() === FALSE  ) 
			{
				$this->load->view('templates/headerB', $data);
				$this->load->view('news/create');
				$this->load->view('upload/upload_form');
				$this->load->view('templates/footer');
			} 
			else 
			{
				$this->news_model->set_news();
				$this->load->view('news/success');
							
			}
		}  
		else
		{
			redirect('login');
		}
	}
}
