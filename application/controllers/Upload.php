<?php

class Upload extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
		$this->load->model('upload_model');
		$this->load->model('series_model');
		$this->load->model('videos_model');
		$this->load->helper(array('form', 'url', 'directory'));
		$this->load->library('form_validation');
  }

	// loads the index
  public function index()
  {
		$this->CheckUser();		

		$data['series'] = $this->upload_model->get_series();
		$data['videos'] = $this->upload_model->get_videos();
		

		$this->load->view('templates/headerB');
    $this->load->view('upload/index', $data);
		$this->load->view('templates/footer');

	}

	// loads the serie modification page if slug is passed
	public function view($slug = NULL)
	{
		$this->CheckUser();		

		$data['serie'] = $this->series_model->get_serie($slug);
		$data['serie']['photos'] = $this->upload_model->browse_files($data['serie']['nom_fichiers']);

		if (empty($data['serie'])) 
		{
			show_404();
		}

		$data['titre'] = $data['serie']['titre'];

		$this->load->view('templates/headerB', $data);
		$this->load->view('upload/view', $data);
		$this->load->view('templates/footer');
	}

	public function view_videos($slug = NULL, $msg = NULL)
	{
		$this->CheckUser();

		$file = "videos-thumbnails/$slug";

		$data['video'] = $this->videos_model->get_video($slug);
		$data['video']['thumbs'] = $this->upload_model->browse_files($file);
		$data['msg'] = $msg;

		$this->load->view('templates/headerB', $data);
		$this->load->view('upload/video', $data);
		$this->load->view('templates/footer');
	}
	
	// loads the page to create a new serie
	public function new_serie ()
	{
		$this->load->view('templates/headerB');
		$this->load->view('upload/new');
		$this->load->view('templates/footer');
	}

	public function new_video ()
	{
		$this->load->view('templates/headerB');
		$this->load->view('upload/newvideo');
		$this->load->view('templates/footer');
	}

	// creates a new video page in the database
	public function add_video()
	{
		// checks if admin is logged in
		$this->CheckUser();		

		// creates the form validation rules
		$config = array(
			array(
				'field' => 'name',
				'label' => 'name',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			),
			array(
				'field' => 'name-en',
				'label' => 'name-en',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			),
			array(
				'field' => 'link',
				'label' => 'link',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			)
		);

		// checks if the form submitted is following the rules
		$this->form_validation->set_rules($config);	

		// if not, loads the form again
		if ($this->form_validation->run() === FALSE)
		{			
			$this->load->view('templates/headerB');
			$this->load->view('upload/newvideo');
			$this->load->view('templates/footer');		  
		} 
		// else writes the new page's values in database, uploads the new thumbnail on the server the adds the name of the thumbnail in database, 
		// the redirects to the backoffice
		else 
		{
			$slug = $this->videos_model->new();
			$msg['txt'] = "The video page has been created with the following message :";
			$msg['thumbresult'] = $this->upload_video_thumbnail($slug);

			
			$this->videos_model->new_thumb($msg['thumbresult']['meta']['image_metadata']['file_name']);

			$this->view_videos($slug, $msg);
		}	
	}

	// deletes a video link in the backoffice, the user part of the site and the database
	public function delete_video()
	{
		$this->CheckUser();		

		$slug = $_GET['slug'];
		$this->videos_model->delete($slug);
		redirect('upload');
	}

	// loads the page to add pictures to a serie
	// accessed after creating a new serie or via serie editing 
	public function add_pictures ($data = NULL)
	{
		$this->CheckUser();		

		$data['serie'] = $_GET['slug'];

		if (empty($data['serie'])) 
		{
			show_404();
		} 
		else 
		{
			$this->load->view('templates/headerB');
			$this->load->view('upload/add', $data);
			$this->load->view('templates/footer');

		}
	}

	// checks the form, sends infos to model, loads 'add' page
	public function create() 
	{
		$this->CheckUser();		

		// creates the rules of the form, in order for them to be checked later on
		$config = array(
			array(
					'field' => 'titre',
					'label' => 'titre',
					'rules' => 'required',
					'errors' => array(
						'required' => 'You must provide a %s.')
			),
			array(
				'field' => 'titre-en',
				'label' => 'titre-en',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
		),
			array(
				'field' => 'slug',
				'label' => 'slug',
				'rules' => 'required',
				'errors' => array(
					'required' => 'you absolutely MUST provide a %s.')
				),
				array(
					'field' => 'nom_fichiers',
					'label' => 'nom_fichiers',
					'rules' => 'required',
					'errors' => array(
						'required' => 'you absolutely MUST provide a %s.')
					),
			array(
					'field' => 'mep',
					'label' => 'Type de mise en page',
					'rules' => 'required',
					'errors' => array(
						'required' => 'You must choose a %s.')
				)
		);
		$this->form_validation->set_rules($config);	

		$slug = $this->input->post('slug');

		if ($this->form_validation->run() === FALSE)
		{			
			$this->load->view('templates/headerB');
			$this->load->view('upload/new');
			$this->load->view('templates/footer');
		} 
		else 
		{
			$this->upload_model->create();			
			
			redirect('upload/add?slug=' . $slug);

		}
	}

	public function update()
	{
		$this->CheckUser();		

		$this->load->library('form_validation');

		$config = array(
			array(
							'field' => 'titre',
							'label' => 'titre',
							'rules' => 'required',
							'errors' => array(
								'required' => 'You must provide a %s.')
			),
			array(
							'field' => 'thumbnail',
							'label' => 'Miniature',
							'rules' => 'required',
							'errors' => array(
								'required' => 'You must provide a %s.')
			)			
		);

		$this->form_validation->set_rules($config);	

		if ($this->form_validation->run() === FALSE)
		{			
			// header('location:'.$_SERVER['HTTP_REFERER']);		   

		} 
		else 
		{
			$slug = $this->upload_model->update();
			redirect('upload/' . $slug);
		}
	}

	public function update_video()
	{
		$this->CheckUser();		

		$this->load->library('form_validation');

		$config = array(
			array(
				'field' => 'name',
				'label' => 'name',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			),
			array(
				'field' => 'name-en',
				'label' => 'name-en',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			),
			array(
				'field' => 'link',
				'label' => 'link',
				'rules' => 'required',
				'errors' => array(
					'required' => 'You must provide a %s.')
			)
		);

		$this->form_validation->set_rules($config);	

		if ($this->form_validation->run() === FALSE)
		{			
		  header('location:'.$_SERVER['HTTP_REFERER']);		  
			echo "form validation failed.";
		} 
		else 
		{
			$slug = $this->videos_model->update();
			if (($this->input->post('userfile')) != NULL && $this->input->post('userfile') != "") 
			{
				$error = $this->upload_video_thumbnail($slug);

				var_dump($error);
				var_dump($this->input->post('userfile'));
				$_FILES = $this->input->post('userfile');
				var_dump($_FILES);
			}
			redirect('upload/video/' . $slug);
		}	
	}

	public function delete()
	{
		$this->CheckUser();		

		$slug = $_GET['slug'];
		$this->upload_model->delete($slug);
		redirect('upload');
	}
	
	public function uploads()
	{
		$this->CheckUser();		

		$slug = $_GET['slug'];
		$serie = $this->series_model->get_serie($slug);
		$file = $serie['nom_fichiers'];


		// if there are files to upload
		if (!empty($_FILES['file']['name'])) 
		{
			$filesCount = count($_FILES['file']['name']);
			for ($i = 0; $i < $filesCount; $i++) 
			{
				$_FILES['uploadFile']['name'] = str_replace(",","_",$_FILES['file']['name'][$i]);
				$_FILES['uploadFile']['type'] = $_FILES['file']['type'][$i];
				$_FILES['uploadFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
				$_FILES['uploadFile']['error'] = $_FILES['file']['error'][$i];
				$_FILES['uploadFile']['size'] = $_FILES['file']['size'][$i];

				//Directory where files will be uploaded
				$uploadPath = 'uploads/' . $file;
				$config['upload_path'] = $uploadPath;

				// renames the files 
				$config['file_name'] = $slug;

				// Specifying the file formats that are supported.
				$config['allowed_types'] = 'jpg|png|jpeg|gif';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ($this->upload->do_upload('uploadFile')) 
				{
					$fileData = $this->upload->data();
					$uploadData[$i]['file_name'] = $fileData['file_name'];
				}
			}
			if (!empty($uploadData)) 
			{
				$list=array();
				foreach ($uploadData as $value) 
				{
					array_push($list, $value['file_name']);
				}
				echo json_encode($list);			
			}
		}
	}

	public function home()
	{		
		$this->CheckUser();		

		$data['series'] = $this->upload_model->get_series();
		
		for ($i = 0 ; $i < sizeof($data['series']) ; $i++)
		{
			$data['series'][$i]['photos'] = $this->upload_model->browse_files($data['series'][$i]['nom_fichiers']);
		}
		
		$this->load->view('templates/headerB');
		$this->load->view('upload/home', $data);
		$this->load->view('templates/footer');
	}

	public function randHome()
	{
		$data = $_POST;

		$this->upload_model->homeImg($data);
	}

	public function resetHome()
	{
		$this->CheckUser();		

		$this->upload_model->resetHome();
	}

	public function CheckUser()
	{
		$userdata = $this->session->userdata;
		if ($userdata['user'] != 'Risu' || $userdata['email'] != 'l-molinier@outlook.fr' || $userdata['logged_in'] != TRUE)
		{
			redirect('login');
		}
	}

	public function ReorderImages()
	{
		$data['slug'] = $_POST['slug'];
		$data['imagesOrder'] = $_POST['imagesOrder'];

		$result = $this->upload_model->ReorderImages($data);			
		
		if($result == "success")
			echo json_encode(true);
	  else 
			echo json_encode(false);
	}

	public function reorderSeries()
	{
		$data['ordre'] = $_POST['seriesOrder'];

		$result = $this->upload_model->UpdateSerieOrder($data);
		
		if($result)
			echo json_encode(true);
		else 
			echo json_encode(false);
	}

	public function upload_video_thumbnail($slug) 
	{
		if (!is_dir('./uploads/videos-thumbnails/'.  $slug)) {
			mkdir('./uploads/videos-thumbnails/' .  $slug, 0777, TRUE);
		} 

		$config['upload_path'] = './uploads/videos-thumbnails/' . $slug;
		$config['file_name'] = $slug;
		$config['allowed_types'] = 'jpg|png';
		$config['max_size'] = 2500;
		$config['max_width'] = 3000;
		$config['max_height'] = 2000;

		$this->load->library('upload', $config);


		if (!$this->upload->do_upload('userfile')) 
		{
			$error = array('error' => $this->upload->display_errors());

			return $error;
		} 
		else 
		{
			$data['meta'] = array('image_metadata' => $this->upload->data());

			$ext = $data['meta']['image_metadata']['file_ext'];

			// config for image resize
			$config = array();
			$config['image_library']  = 'gd2';
			$config['maintain_ratio'] = TRUE;
			$config['create_thumb']   = false;
			$config['source_image']   = "./uploads/videos-thumbnails/$slug/$slug" . $ext;
			$config['width']          = 300;
			$config['height'] 				= 200;
			
			echo $config['source_image'];

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$data['upload'] = array('upload_data' => $this->upload->data());


			return $data;
		}
  }
}
?>
