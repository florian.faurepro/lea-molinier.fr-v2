<?php
class Series_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	// get every active serie
	public function get_series()
	{                       
		$where = "active = 1";
		
		$this->db->order_by('ordre', 'ASC');
		$query = $this->db->get_where('serie', $where);
		return $query->result_array();
	}
	
	// go fetch the serie(s) !
	public function get_serie($slug = FALSE)
	{
		// if no slug specified, get every active series
		if ($slug === FALSE)
		{
			$where = "active = 1";
			$query = $this->db->get_where('serie', $where);
			return $query->result_array();
		}
		// else get the corresponding serie
		$query = $this->db->get_where('serie', array('slug' => $slug));
		return $query->row_array();
	}

	public function get_randy()
	{
		$query = $this->db->get('homerandomimages');
		return $query->result_array();
	}

	// maps the serie's upload directory
	public function browse_files($file)
	{
		$this->load->helper('directory');

		$result = directory_map('./uploads/' . $file, 1);
		// natcasesort($result);
		return  $result;
	}
}