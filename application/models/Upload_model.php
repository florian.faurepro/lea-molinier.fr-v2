<?php
class Upload_model extends CI_Model {
	
	public function __construct()
  {
		$this->load->database();
		$this->load->helper('directory');	
		$this->load->helper("file");

  }

  // go fetch the series !
  public function get_series()
  {
		$this->db->order_by('ordre', 'ASC');
    $query = $this->db->get_where('serie');
    return $query->result_array();
	}
	
	// go fetch the videos !
	public function get_videos()
  {
		$this->db->order_by('ID', 'ASC');
    $query = $this->db->get_where('videos');
    return $query->result_array();
  }
	
	// sends the new serie's values to database
	public function create()
	{
		$this->load->helper('url');

		// translate the checkbox' value in a bool
		if($this->input->post('active') == 'on') $active = '1';
		else $active = '0';

		$data = array (
			'active' => $active,
			'titre' => $this->input->post('titre'),
			'titre-en' => $this->input->post('titre-en'),
			'slug' => $this->input->post('slug'),
			'description' => $this->input->post('description'),
			'description-en' => $this->input->post('description-en'),
			'nom_fichiers' => $this->input->post('nom_fichiers'),
			'mise_en_page' => $this->input->post('mep')
		);
		
		// creates the serie's directory if it does not exists
		if (!is_dir('./uploads/'. $data['nom_fichiers'])) {
			mkdir('./uploads/' . $data['nom_fichiers'], 0777, TRUE);
		} 

		$this->db->insert('serie', $data);

		return $data;
	}

	// sends the  serie's new values to database
	public function update()
  {
    $this->load->helper('url');    
		$title = $this->input->post('titre');
		$slug = $this->Slugify($title);
		$id = $this->input->post('serieId');
		$prevFileName = $this->PreviousTitle($id);
				
		// translate the checkbox's value into an int -- ACTIVE --
		if($this->input->post('active') == 'on') 
			$active = '1';
		else 
			$active = '0';

		// Adds the new values to the -- FINAL VALUES ARRAY --
		$data = array(
			'id' => $id,
			'titre' => $title,
			'titre-en' => $this->input->post('titre-en'),
			'slug' => $slug,
			'description' => $this->input->post('description'),
			'description-en' => $this->input->post('description-en'),
			'active' => $active,
			'thumbnail' => $this->input->post('thumbnail'),
			'nom_fichiers' => $slug,
			'mise_en_page' => $this->input->post('mep')
		);
		
		// Sends the -- DATAS TO DB --
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->update('serie', $data);
		
		// Change folder name if necessary
		$this->RenameFolder($slug, $prevFileName);

		return $slug;
	}

	// Delete the serie in database and the serie's folder containing the photos
	public function delete($slug)
	{
		$this->db->delete('serie', array('slug' => $slug));
		$this->NukeFolderAndContent($slug);
	}

	// Add photos used in homePage into database
	public function homeImg($data)
	{
		for ($i = 0; $i < sizeof($data['photos']) ; $i++ )
		{
			$dataFormatted[$i] = array(
				'src' => $data['photos'][$i],
				'slug' => $data['slug'][$i]
			);
			$this->db->insert('homerandomimages', $dataFormatted[$i]);
		}
	}

	// Clears Homepage Datas in DB
	public function resetHome()
	{
		$this->db->empty_table('homerandomimages');
	}
	
	// fetch the content of a given folder from the uploads
	public function browse_files($file)
	{
		return directory_map('./uploads/' . $file);		
	}
	
  // clears the string to create the -- SLUG --
	public function Slugify(string $title)
	{
		$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u, ,.,/,_,'");	
		$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,-,-,-,-,-");
		$slug = strtolower(trim(str_replace($search, $replace, $title)));

		return $slug;
	}

	// Creates new folder, moves content of origin folder then destroys origin folder
	public function RenameFolder(string $slug, string $prevFileName)
	{
		if ($prevFileName != $slug)
		{
			$prevPath = "./uploads/" . $prevFileName;
			$filePath = "./uploads/" . $slug;
	
			rename($prevPath, $filePath);		
		}
	}

	// Deletes the serie's folder containing the photos
	public function NukeFolderAndContent(string $prevFileName)
	{
		$prevPath = "./uploads/" . $prevFileName  . "/";
		delete_files($prevPath, TRUE);
		rmdir($prevPath); 
	}

	// fetch the title of the modified serie
	public function PreviousTitle(int $id)
	{
		$query = $this->db->select('slug')
											->where('id', $id)
											->get_where('serie');

		foreach($query->row_array() as $row){
			$previousTitle = $row;
		}

		return $previousTitle;
	}

	public function ReorderImages($data)
	{
		$slug = $data['slug'];
		$imagesOrder = $data['imagesOrder'];
		$files = $this->browse_files($slug);
		$path = './uploads/' . $slug . "/";
		$tempPath = $path . 'temp/';
		$x = 0;

		try
		{
			mkdir($tempPath);
	
			for($i = 0; $i < sizeof($imagesOrder); $i++)
			{
				rename($path . $imagesOrder[$i], $tempPath . 'temp-' . $i . ".jpg");
			}	

			$tempFiles = $this->browse_files($slug . "/temp");
			natcasesort($tempFiles);
	
			foreach	($tempFiles as $file)
			{
				rename($tempPath . $file, $path . $x . ".jpg");
				$x++;
			}	
			rmdir($tempPath);

			$result = "success";

			return $result;
		}

		catch (Exception $e)
		{
			$error = "Error : " . $e->getMessage();
			return $error;
		}
	}

	public function UpdateSerieOrder($ordre)
	{
		$x=0;
		foreach	($ordre['ordre'] as $i)
		{
			$this->db->set('ordre', $x);
			$this->db->where('ID', $i);
			$this->db->update('serie');
			$x++;
		}
		return true;
	}

	public function SearchArray($value, $array) 
	{ 
			return(array_search($value, $array)); 
	} 
}
