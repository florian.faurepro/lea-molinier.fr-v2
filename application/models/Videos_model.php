<?php
class Videos_model extends CI_Model {
	
	public function __construct()
  {
		$this->load->database();
		$this->load->helper('directory');	
		$this->load->helper("file");

  }

  // go fetch the videos !
  public function get_videos()
  {
		$this->db->order_by('ID', 'ASC');
    $query = $this->db->get_where('videos', array('active' => '1'));
    return $query->result_array();
  }

  public function get_video($slug = NULL)
  {
    if($slug == NULL)
    {
      return NULL;
    }
    $query = $this->db->get_where('videos', array('slug' => $slug));
    return $query->row_array();
  }

  public function new()
  {
    $this->load->helper('url');    
		$name = $this->input->post('name');
		$nameen = $this->input->post('name-en');
    $slug = $this->Slugify($name);
				
		// translate the checkbox's value into an int -- ACTIVE --
		if($this->input->post('active') == 'on') 
			$active = '1';
		else 
			$active = '0';

		// Adds the new values to the -- FINAL VALUES ARRAY --
		$data = array(
			'name' => $name,
			'name-en' => $nameen,
      'slug' => $slug,
      'link' => $this->input->post('link'),
			'description' => $this->input->post('description'),
			'active' => $active,
			'thumbnail' => $slug,
		);
		
		// Sends the -- DATAS TO DB --
		$this->db->set($data);
		$this->db->insert('videos', $data);
		
		// Change folder name if necessary
		// $this->RenameFolder($slug, $prevFileName);

		return $slug;    
	}

	public function new_thumb($thumb)
	{
		$data = array('thumbnail' => $thumb);
		$this->db->set($data);
		$this->db->update('videos', $data);
	}
	
  public function update()
  {
    $this->load->helper('url');    
		$name = $this->input->post('name');
		$nameen = $this->input->post('name-en');
    $slug = $this->Slugify($name);
    $id = $this->input->post('id');
		// $prevFileName = $this->PreviousTitle($id);
				
		// translate the checkbox's value into an int -- ACTIVE --
		if($this->input->post('active') == 'on') 
			$active = '1';
		else 
			$active = '0';

		// Adds the new values to the -- FINAL VALUES ARRAY --
		$data = array(
			'name' => $name,
			'name-en' => $nameen,
      'slug' => $slug,
      'link' => $this->input->post('link'),
			'description' => $this->input->post('description'),
			'description-en' => $this->input->post('description-en'),
			'active' => $active,
			'thumbnail' => $this->input->post('thumbnail'),
		);
		
		// Sends the -- DATAS TO DB --
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->update('videos', $data);
		
		// Change folder name if necessary
		// $this->RenameFolder($slug, $prevFileName);

    return $slug;  
  }

	public function delete($slug)
	{
		$this->db->delete('videos', array('slug' => $slug));
		$this->NukeFolderAndContent($slug);
	}

  // clears the string to create the -- SLUG --
	public function Slugify(string $title)
	{
		$search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u, ,.,/,_,'");	
		$replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,-,-,-,-,-");
		$slug = strtolower(trim(str_replace($search, $replace, $title)));

		return $slug;
	}

	// Deletes the serie's folder containing the photos
	public function NukeFolderAndContent($prevFileName)
	{
		$prevPath = "./uploads/videos-thumbnails/" . $prevFileName  . "/";
		delete_files($prevPath, TRUE);
		rmdir($prevPath); 
	}
}