<?php
class Home_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }
  
  // go fetch the serie(s) !
	public function get_serie($slug)
	{		
		$query = $this->db->get_where('serie', array('slug' => $slug));
		return $query->row_array();
  }
}