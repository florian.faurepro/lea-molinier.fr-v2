<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['reorderSeries'] = 'upload/reorderSeries';
$route['series/(:any)'] = 'series/view/$1';
$route['series'] = 'series';
$route['videos/(:any)'] = 'videos/view/$1';
$route['videos'] = 'videos';
$route['login'] = 'login';
$route['login/index'] = 'login';
$route['upload/home'] = 'upload/home';
$route['upload/resetHome'] = 'upload/resetHome';
$route['upload/ReorderImages'] = 'upload/ReorderImages';
$route['upload/randHome'] = 'upload/randHome';
$route['upload/delete'] = 'upload/delete';
$route['upload/delete_video'] = 'upload/delete_video';
$route['upload/uploads'] = 'upload/uploads';
$route['upload/add'] = 'upload/add_pictures';
$route['upload/update'] = 'upload/update';
$route['upload/new'] = 'upload/new_serie';
$route['upload/create'] = 'upload/create';
$route['upload/update_video'] = 'upload/update_video';
$route['upload/add_video'] = 'upload/add_video';
$route['upload/new_video'] = 'upload/new_video';
$route['upload/video/(:any)'] = 'upload/view_videos/$1';
$route['upload/(:any)'] = 'upload/view/$1';
$route['upload'] = 'upload';
$route['(:any)'] = 'pages/view/$1';
$route['default_controller'] = 'pages/view';
