(function(){
var data = null,
	callback = null,
	called = false;

window.promiseMeData = function(myCallback){
	callback = myCallback;
	if( data && !called ){
		called = true;
		callback(data);
	}
};

var r;
if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	r = new XMLHttpRequest();
} else if (window.ActiveXObject) { // IE
	try {
		r = new ActiveXObject('Msxml2.XMLHTTP');
	} catch (e) {
		try {
			r = new ActiveXObject('Microsoft.XMLHTTP');
		} catch (e) {}
	}
}
if( r.overrideMimeType !== undefined ){
	r.overrideMimeType('application/json');
}
//r.responseType = 'json'; // does not work in IE11
r.open('GET', window.loadUrl, true);
r.send(null);
r.addEventListener("load", function(e){
	data = JSON.parse(r.responseText);
	if( !called && callback ){
		called = true;
		callback(data);
	}

}, false);
})();
