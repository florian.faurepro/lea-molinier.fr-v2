-- MySQL dump 10.16  Distrib 10.1.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lea
-- ------------------------------------------------------
-- Server version	10.1.41-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `lea`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lea` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `lea`;

--
-- Table structure for table `homerandomimages`
--

DROP TABLE IF EXISTS `homerandomimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homerandomimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(99) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `src` (`src`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homerandomimages`
--

LOCK TABLES `homerandomimages` WRITE;
/*!40000 ALTER TABLE `homerandomimages` DISABLE KEYS */;
INSERT INTO `homerandomimages` VALUES (40,'peregrinations-photographiques/4.jpg','peregrinations-photographiques'),(41,'peregrinations-photographiques/10.jpg','peregrinations-photographiques'),(42,'peregrinations-photographiques/15.jpg','peregrinations-photographiques'),(43,'metamorphose/4.jpg','metamorphose'),(44,'terre-de-legendes/Molinier_Rep_09.jpg','terre-de-legendes'),(45,'dissolution/8.jpg','dissolution');
/*!40000 ALTER TABLE `homerandomimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serie`
--

DROP TABLE IF EXISTS `serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(99) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `description` varchar(9999) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `thumbnail` varchar(20) NOT NULL DEFAULT '0',
  `mise_en_page` int(1) NOT NULL,
  `nom_fichiers` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `titre` (`titre`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serie`
--

LOCK TABLES `serie` WRITE;
/*!40000 ALTER TABLE `serie` DISABLE KEYS */;
INSERT INTO `serie` VALUES (21,'Mirage Urbain','Mirage-Urbain','La série Mirage Urbain est à regarder comme une fiction. Celle d’une ville désertée par ses habitants lors d’un été caniculaire. Les seuls humains restants sont en proie à une fatigue écrasante qui les abandonne assoupis dans les rues. \r\n\r\nVision onirique d’une ville au ralenti où le privé et le confort se mêlent à la dureté du béton de l’espace public. \r\n\r\nFin de l’utopie de la cité idéale qui concentre la production des richesses culturelles, économiques. Dans ses envies de grandeurs architecturales, l’homme s’est exclu lui-même du paysage qu’il a construit. La nature est relégué au second plan, artificielle. \r\n\r\nSeul le soleil, implacable, nous rappelle par sa violence silencieuse notre modeste place d’êtres éphémères dans un univers qui continue à avancer, avec ou sans nous.',1,'1.jpg',0,'mirage-urbain'),(22,'Métamorphose','Metamorphose','La féminité comme une transformation. \r\nSortir de la chrysalide ne se fait cependant pas sans douleur. \r\nIl existe ce lien ténu que je ne saurais expliquer. \r\nIl y a la biologie, et il y a l\'émotion. \r\nMais une fois hors de la membrane, les contours restent encore à définir.',1,'7.jpg',0,'metamorphose'),(23,'La Vigane','La-Vigane','',1,'1.jpg',0,'la-vigane'),(24,'Terre de légendes','Terre-de-legendes','L\'Islande, pays insulaire reculé au Sud du cercle polaire arctique.\r\n\r\nLà où la nature fait rage autant que de miracles, des dizaines de générations ont peuplé les paysages d\'êtres féériques ou effrayants qui vivent en symbiose avec les éléments.\r\n\r\nA l\'heure de la globalisation, les Islandais, s\'ils se prêtent volontiers aux blagues sur les elfes des journalistes étrangers, gardent un certain respect pour les traditions orales de leurs ancêtres et la manière dont elles habitent encore les grands espaces glacés.\r\n\r\nJe remercie sincèrement le Professeur Terry Gunnell ainsi que Ragnhildur Jónsdóttir pour leur aide généreuse dans mon apprentissage de la culture islandaise et de ses merveilles.',1,'Molinier_Rep_01.jpg',0,'terre-de-legendes'),(29,'Intime Violence ','Intime-Violence-','J\'ai rencontré 5 femmes. Elles m\'ont ouvert les portes de leurs quotidiens, en même temps que celles de leurs passés. \r\nToutes ont subi des violences de la part d\'un proche : ami, compagnon, membre de la famille... \r\nAprès le traumatisme, il faut tout reconstruire. Un processus lent et complexe s\'engage ; les autres ont souvent tendance à le nier. On a le droit d\'être une victime, mais pas trop longtemps...\r\nAlors elles mènent leur combat chaque jour, en silence. Elles rapiècent le temps et la mémoire, avec le fil de la résilience. \r\nJ\'ai voulu savoir ce qui faisait leur force et j\'ai été impressionnée par leur douceur.\r\nLeur confiance durant la réalisation de ce projet fut d\'un courage et d\'une générosité sans faille. Je remercie grandement ces 5 magnifiques personnes d\'avoir accepté de partager leur histoire, pour que la souffrance ne soit plus jamais tue.\r\n\r\n',1,'Molinier_WIP_1.jpg',0,'intime-violence'),(25,'Le Grand Fauconnier','Le-Grand-Fauconnier','Chaque année au mois de juillet, le commité des fêtes du Grand Fauconnier organise à Cordes-sur-Ciel (81170) des fêtes médiévales dans un cadre exceptionnel. \r\n\r\nA cette fin, les bénévoles de la costumerie ainsi qu\'une costumière de métier travaillent toute l\'année pour proposer aux festivaliers des costumes entièrement faits à la main et réalisés avec une véritable recherche de fidélité aux éléments historiques.\r\n\r\nEntre ces murs de pierre j\'ai eu la chance de réaliser ce reportage et de vivre des moments de création et de complicité avec des femmes désireuses de partager leur passion de la couture.\r\n\r\n',1,'20.jpg',0,'le-grand-fauconnier'),(27,'Petits pois Coquillettes','Petits-pois-Coquillettes','Extraits du livre\r\n\r\nLe journal de deux enfances qui se croisent : celle présente d\'Emma 7ans et la mienne, passée sous la brume de la mémoire. Le brouillon des émotions, les dents de lait qui bougent, les petits pois qui roulent sous la table... \r\n\r\nA travers l\'exploration de différents medium (photographie numérique et argentique, collage, montage, peinture, écriture...) nous avons tenté de fixer la fugacité du \"grandir\" entre ces quelques pages, comme nous l\'avons fait aussi avec des fleurs séchées. \r\n\r\nJe remercie la famille d\'Emma pour sa confiance et son soutient dans le projet. Et j\'espère te revoir bientôt, petite fée, que tu remettes des barrettes à paillettes dans mes cheveux emmêlés.',1,'EmmaSite4.jpg',0,'petits-pois-coquillettes'),(20,'Pérégrinations photographiques','Peregrinations-photographiques','',1,'15.jpg',0,'peregrinations-photographiques'),(34,'Dissolution','dissolution','',1,'1.jpg',0,'dissolution');
/*!40000 ALTER TABLE `serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'10f60336','12b802ee');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-22 11:37:04
